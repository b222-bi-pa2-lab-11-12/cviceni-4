# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables
SOURCE_EXT = .cpp
HEADER_EXT = .h

CXX = g++
LD = g++

CXXFLAGS = -Wall -pedantic -g -fsanitize=address -std=c++17
LD_FLAGS = $(CXXFLAGS)

SOURCE_FILES = CFraction.cpp testFunctions.cpp
OBJECTS = $(patsubst %$(SOURCE_EXT),%.o,$(SOURCE_FILES))

all: test-all
	echo "Done"

run: test-all
	./$<

run-%: test-%
	./$<

test-%: test%.o $(OBJECTS)
	$(LD) $^ $(LD_FLAGS) -o $@

test-all: all.o $(OBJECTS)
	$(LD) $^ $(LD_FLAGS) -o $@

test-warmup: warmup.o $(OBJECTS)
	$(LD) $^ $(LD_FLAGS) -o $@

%.o:
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -rf *.o test-*


# find . -type f -name "*.cpp" -exec gcc -MM {} \;
# Generated dependencies
CFraction.o: operators/CFraction.cpp operators/CFraction.h

all.o: operators/tests/all.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test1.o: operators/tests/test1.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test2.o: operators/tests/test2.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test3.o: operators/tests/test3.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test4.o: operators/tests/test4.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test5.o: operators/tests/test5.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test6.o: operators/tests/test6.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test7.o: operators/tests/test7.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
test8.o: operators/tests/test8.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
testFunctions.o: operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h
warmup.o: operators/tests/warmup.cpp operators/tests/testFunctions.cpp \
 operators/tests/../CFraction.h

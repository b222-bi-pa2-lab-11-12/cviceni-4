# cvičení 4 https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B

## Úvod
Cílem dnešního cvičení je si vyzkoušet přetěžování operátorů. Jedná se o vlastnost C++, která je testována nejen v domácích
úlohách, ale i na zkoušce. V rámci celého cvičení bude pouze 1 úloha.

Cílem této úlohy je vytvořit třídu `CFraction`, jejiž instance bude reprezentovat 1 zlomek
v našem programu, zlomek půjde sčítat, odčítat, násobit, dělit, modulit a porovnávat.

U všech nově vytvořených metod v celé úloze se dobře zamyslete, zda mohou (musí) být konstantní, nebo ne.

## Git
Naklonujte si prosím tento repozitář, v rámci celého cvičení s ním budeme pracovat
```
git clone https://gitlab.fit.cvut.cz/b222-bi-pa2-lab-11-12/cviceni-4.git
```

## Úloha má připravený makefile
Jeho použití je velmi jednoduché, stačí spustit příkaz `make` a za ten zadat target, který chceme spustit.<br>
V našem případě je to `make all`, který spustí kompilaci všech zdrojových souborů a vytvoří spustitelný soubor `test-all`.<br>
Popřípadě existují targety `run` a `run-1`, `run-2`, ... `run-8`, které spustí testy jednotlivých pod úloh.<br>
V případě, že chcete spustit testy pro úlohu 1, stačí zadat `make run-1` atd.<br>
Jako první si vyzkoušejte `make run-warmup` viz níže.<br>
Jestli máte nějaké problémy s makefilem, neváhejte se zeptat.<br>

## Něco na rozehřátí
#### Vytvořte třídu `CFraction`
Ve složce [operators](operators) vytvořte soubory `CFraction.h` a `CFraction.cpp`.
V těchto souborech vytvořte třídu `CFraction`, která prozatím bude mít metody `getNumerator`,
`getDenominator`, konstruktor přijímající 2 argumenty (čitatel a jmenovatel, v tomto pořadí),
konstruktor prijimající 1 argument (hodnota zlomku jako celé číslo)
a konstruktor nepřijímající argumenty (zlomek bude v tu chvíli držet hodnotu 1).

#### tip: Všechny 3 konstruktory mohou být rálně reprezetovány pouze jedním, zkuste přijít na to jak.

## Úloha na toto cvičení
### 1) Proveďte přetížení binárních operátorů `+=`, `-=`, `*=`, `/=` a `%=`
Pokud bude provedena jedna z výše uvedených operací mezi 2 instancema `CFraction`, například:
```c++
    CFraction x(1,2), y(3,2);
    x += y;
```
tak chceme, aby program se choval přesně tak jako očekáváme. To v našem přikladě znamená, že hodnota zlomku
`x` bude zvýšena o hodnotu zlomku `y`. Nezapomeňte, že samotný výsledek operátoru musí vrátit referenci na
hodnotu před ním.

Není nutné, aby zlomek byl minimalizován (aby se ze 2/4 se stala 1/2).

### 2) Pokud je to nutné, vytvořte kopírovací konstruktor a zároveň přetižení operátoru `=`

### 3) Proveďte přetížení binárních operátorů `+`, `-`, `*`, `/` a `%`
Pokud bude provedena jedna z výše uvedených operací mezi 2 instancema `CFraction`, například:
```c++
    CFraction x(1,2), y(3,2), z;
    z = (x + y);
```
tak chceme, aby program se choval přesně tak jako očekáváme. To v našem přikladě znamená, že hodnota zlomku
`x` a `y` bude nezmeněná, ale pouze jejich součet se ocitne ve zlomku `z`.

Opět není nutná minimalizace.

### 4) Proveďte přetížení prefixových unárních operátorů `++` a `--`
Tyto operátory opět vrací referenci na upravované objekt.

### 5) Přidejte možnost konverze na `int` a `double`
```c++
    CFraction x(3,2);
    cout << (double) x << endl; // 1.5
    cout << (int) x << endl; // 1
```

### 6) Proveďte přetížení binárních operátorů pro porovnání (`==`, `!=`, `<`, `>`, `<=` a `>=`)
Všechny tyto operátory budou vracet `bool`. Pozor však na fakt, že `1/2` a `2/4` se sobě musí rovnat!!!

### 7) Proveďte přetížení operátoru `<<` a `>>`.
Operátory je nutné udělat tak, aby byl schopen číst(zapisovat) z(do) `std::cin`(`std::cout`), tak z(do) otevřeného souboru. Operátory musí jít zřetězit (`cin >> x >> y`). 
Výstup/vstup je ve formátu `NUMERATOR/DENOMINATOR` (např. `2/5`). Tento formát bude dodržen, i přes to, že by se zlomek dal zapsat jako celé číslo. Při čtení, pokud došlo k chybě, (jedno z čísel není číslo, čísla neodděluje lomítko), bude stream nastaven do chybného stavu.

Příklad použití:
```c++
    CFraction x, y;
    istringstream s("1/2 2/5");
    s >> x >> y;
    cout << y " - " << x << " = " << y - x << endl;
```

### 8) BONUS - házení vyjímek
Během libovolné z výše zmíněných operací může dojít k dělení 0. To se c++ nelíbí, a dá nám to vědět pádem programu. 
Vytvořte si novou třídu `EDivisionByZero` (v hlavičkovém souboru `CFraction.h`) a její instaci vracejte ve chíli, kdyby k tomu mělo dojít.

Případy, kdy k dělení nulou dojít může, ale dá se mu šikovnějšími operacemi zabránit vyjímku nevyhazujte, pouze program poupravte.

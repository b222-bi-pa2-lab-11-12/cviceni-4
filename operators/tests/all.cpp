
#define __WARMUP__
#define __TEST1__
#define __TEST2__
#define __TEST3__
#define __TEST4__
#define __TEST5__
#define __TEST6__
#define __TEST7__
#define __TEST8__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(
        tests::warmup() &&
        tests::test1() &&
        tests::test2() &&
        tests::test3() &&
        tests::test4() &&
        tests::test5() &&
        tests::test6() &&
        tests::test7() &&
        tests::test8()
    ) {
        cout << "All test finished successfully!!!" << endl;
        cout << "You now can ask for additional point." << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



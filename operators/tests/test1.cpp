

#define __TEST1__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test1()) {
        cout << "Test1 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



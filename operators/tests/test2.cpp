
#define __TEST2__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test2()) {
        cout << "Test2 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



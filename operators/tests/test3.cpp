
#define __TEST3__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test3()) {
        cout << "Test3 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



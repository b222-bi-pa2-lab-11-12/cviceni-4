
#define __TEST4__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test4()) {
        cout << "Test4 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



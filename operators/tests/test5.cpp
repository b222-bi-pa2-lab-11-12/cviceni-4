
#define __TEST5__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test5()) {
        cout << "Test5 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



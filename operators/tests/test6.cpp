
#define __TEST6__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test6()) {
        cout << "Test6 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



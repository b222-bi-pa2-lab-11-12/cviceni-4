
#define __TEST7__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test7()) {
        cout << "Test7 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}




#define __TEST8__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::test8()) {
        cout << "Test8 success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}



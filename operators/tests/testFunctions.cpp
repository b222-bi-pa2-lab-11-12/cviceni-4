#include <cassert>
#include <iostream>
#include <numeric>
#include <sstream>

#include "../CFraction.h"

class tests {
   public:
#ifdef __WARMUP__
    static bool warmup() {
        CFraction f1;
        assert_frac("Default constructors", f1, 1, 1, false);
        assert_frac("Default denominator", CFraction(42), 42, 1, false);
        assert_frac("Both constructor parameters different", CFraction(42, 420),
                    42, 420, false);
        assert_frac("Same constructor parameters", CFraction(55, 55), 55, 55,
                    false);
        return true;
    }
#endif /* __WARMUP__ */

#ifdef __TEST1__
    static bool test1() {
        CFraction f1(2, 5);
        CFraction f2(9, 18);
        f1 += f2;
        assert_frac("First +=", f1, 9, 10);
        assert_frac("Second operand same", f2, 9, 18, false);
        f1 += f2;
        assert_frac("Second +=", f1, 7, 5);
        CFraction f3(2, 5);
        f1 -= f3;
        assert_frac("-=", f1, 1, 1);
        assert_frac("Second operand same", f3, 2, 5, false);
        f1 -= f1;
        assert_frac("-= on itself", f1, 0, 1);
        f2 *= f3;
        assert_frac("*=", f2, 1, 5);
        assert_frac("Second operand same", f3, 2, 5, false);
        f2 /= f3;
        assert_frac("/=", f2, 1, 2);
        assert_frac("Second operand same", f3, 2, 5, false);
        CFraction f4(5, 6);
        CFraction f5(180, 549);
        f4 %= f5;
        assert_frac("%=", f4, 65, 366);
        assert_frac("Second operand same", f5, 180, 549, false);
        const CFraction & fr = f3;
        f1 %= fr;
        f3 += fr;
        f3 -= fr;
        f3 *= fr;
        f3 /= fr;
        f1 += f2 -= f3 *= f4 /= f4 %= f4;
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST2__
    static bool test2() {
        CFraction f1(2, 5);
        CFraction f1_copy(f1);
        assert_frac("Copy constructor", f1_copy, 2, 5, false);
        assert_frac("Copy constructor keeps original", f1, 2, 5, false);
        CFraction f2(6, 7);
        f1 = f2;
        assert_frac("Assignment operator", f1, 6, 7, false);
        assert_frac("Assignment operator keeps original", f2, 6, 7, false);
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST3__
    static bool test3() {
        CFraction f1(2, 5);
        CFraction f2(9, 18);
        CFraction z;
        assert_frac("+", f1 + f2, 9, 10);
        CFraction f3(3, 5);
        assert_frac("-", f2 - f1, 1, 10);
        assert_frac("- to negative", f1 - f3, -1, 5);
        assert_frac("*", f1 * f2, 1, 5);
        assert_frac("/", f1 / f3, 2, 3);
        CFraction f4(5, 6);
        CFraction f5(180, 549);
        assert_frac("%=", f4 % f5, 65, 366);
        const CFraction & fr = f1;
        f4 = fr + fr - fr * fr / fr % fr;
        f4 = f1 + f2 - f3 * f4 / f5 % f5;
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST4__
    static bool test4() {
        CFraction f(1, 2);
        assert_frac("++f", ++f, 3, 2);
        assert_frac("f", f, 3, 2);
        assert_frac("--f", --f, 1, 2);
        assert_frac("f", f, 1, 2);
        ++--f;
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST5__
    static bool test5() {
        CFraction f1(1, 2), f2(7, 5), f3(4, 2), f4(1, 3);
        log("1/2 to int");
        assert((int)f1 == 0);
        log("1/2 to double");
        assert((double)f1 == 0.5);
        log("7/5 to int");
        assert((int)f2 == 1);
        log("7/5 to double");
        assert((double)f2 == 1.4);
        log("4/2 to int");
        assert((int)f3 == 2);
        log("4/2 to double");
        assert((double)f3 == 2.0);
        log("1/3 to int");
        assert((int)f4 == 0);
        log("1/3 to double");
        assert((double)f4 == (1.0 / 3.0));
        const CFraction & fr = f1;
        int x = fr;
        double y = fr;
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST6__
    static bool test6() {
        CFraction f1(2, 5), f2(4, 10), f3(1, 3);
        log("Equality to itself");
        assert(f1 == f1);
        log("Mathematical equality");
        assert(f1 == f2);
        log("Mathematical equality with different signs");
        CFraction f4(-2, 5);
        CFraction f5(2, -5);
        assert(f4 == f5);
        log("Inequality");
        assert(!(f1 == f3));
        assert(f1 != f3);
        log("Lower than with different fractions");
        assert(f3 < f1);
        log("Lower than with the same fraction");
        assert(!(f1 < f1));
        log("Lower than or equal with different fractions");
        assert(f3 <= f1);
        log("Lower than or equal with same fractions");
        assert(f1 <= f2);
        log("Greater than with different fractions");
        assert(f1 > f3);
        log("Greater than with the same fraction");
        assert(!(f1 > f1));
        log("Greater than or equal with different fractions");
        assert(f1 >= f3);
        log("Greater than or equal with same fractions");
        assert(f1 >= f2);
        const CFraction & fr = f1;
        fr == fr;
        fr != fr;
        fr < fr;
        fr > fr;
        fr <= fr;
        fr >= fr;
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST7__
    static bool test7() {
        std::stringstream buf;
        CFraction f1(3, 5), f2(1, 1);
        log("Output 3/5");
        buf << f1;
        assert(buf.str() == "3/5");
        buf.str(std::string());
        log("Output 1/1");
        buf << f2;
        assert(buf.str() == "1/1");
        buf << f2 << f2 << f2 << f2;
        const CFraction & fr = f1;
        buf << f1;
        buf.str(std::string("3/8"));
        CFraction f3;
        buf >> f3;
        assert_frac("Reading fraction 3/8", f3, 3, 8, false);
        return true;
    }
#endif /* __TEST__ */

#ifdef __TEST8__
    static bool test8() {
        CFraction f1(1, 0);
        log("Checking exception on conversion to int");
        bool thrown = false;
        try {
            int x = (int)f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        log("Checking exception on conversion to double");
        try {
            int x = (double)f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        log("Checking exception on equality");
        try {
            bool x = f1 == f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        log("Checking exception on <");
        try {
            bool x = f1 < f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        log("Checking exception on <=");
        try {
            bool x = f1 <= f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        log("Checking exception on >");
        try {
            bool x = f1 > f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        log("Checking exception on >=");
        try {
            bool x = f1 >= f1;
        } catch (EDivisionByZero e) {
            thrown = true;
        }
        assert(thrown);
        thrown = false;
        return true;
    }
#endif /* __TEST__ */

   private:
    static void log(const char *msg) {
        std::cout << "Test: " << msg << std::endl;
    }

    static CFraction minimize(const CFraction &f) {
        int gcd = std::gcd(f.getNumerator(), f.getDenominator());
        int multiplier = 1;
        if (f.getNumerator() > 0 && f.getDenominator() < 0) {
            multiplier = -1;
        }
        return CFraction(f.getNumerator() / gcd * multiplier,
                         f.getDenominator() / gcd * multiplier);
    }

    static void assert_frac(const char *msg, const CFraction &frac,
                            int expected_numerator, int expected_denominator,
                            bool minimize = true) {
        log(msg);
        CFraction minimized(tests::minimize(frac));
        const CFraction &tested = minimize ? minimized : frac;
        assert(tested.getNumerator() == expected_numerator);
        assert(tested.getDenominator() == expected_denominator);
    }
};

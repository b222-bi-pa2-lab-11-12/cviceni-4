
#define __WARMUP__
#include "testFunctions.cpp"
#include <iostream>

using namespace std;

int main() {
    if(tests::warmup()) {
        cout << "Warmup success" << endl;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}


